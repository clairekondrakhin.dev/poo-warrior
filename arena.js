// Import des classes Warrior, WarriorAxe, WarriorSpear, WarriorSword depuis le fichier où elles sont définies
import { Warrior, WarriorAxe, WarriorSpear, WarriorSword } from "./warrior.js";

// round 1 : les guerriers
// Création de deux instances de Warrior
const joan = new Warrior("Joan", 10, 100);
const leon = new Warrior("Leon", 8, 120);

joan.attack(leon);
console.log(leon.life); // Devrait afficher 110 (120-10)

leon.attack(joan);
console.log(joan.life); // Devrait afficher 92 (100-8)

console.log(joan.name + "est " + (joan.isAlive() ? "en vie" : "mort")); // Devrait afficher "Joan est en vie"
// ici on utilise un ternaire si isAlive est true alors on renvoit "en vie" sinon "mort"
//
console.log(leon.name + "est " + (leon.isAlive() ? "en vie" : "mort")); // Devrait afficher "Leon est en vie"

//round 2 : Armes
// Création des instances des warriors
const axeWarrior = new WarriorAxe("Axe Warrior", 12, 90);
const swordWarrior = new WarriorSword("Sword Warrior", 15, 95);
const spearWarrior = new WarriorSpear("Spear Warrior", 10, 85);

joan.attack(axeWarrior);
console.log(axeWarrior.life); // doit afficher 80 soit 90-10
axeWarrior.attack(swordWarrior);
console.log(swordWarrior.life); // doit afficher 71 soit 95-(12*2)
swordWarrior.attack(spearWarrior);
console.log(spearWarrior.life); //doit afficher 55 soit 85-(15*2)

//round 3  : bataille

console.log('nombre de vie avant le combat :' +axeWarrior.life); //80
console.log('nombre de vie avant le combat :' +spearWarrior.life);//55


// création de la boucle de combat entre axeWarrior et spearWarrior ,la boucle continue tant que tous les guerriers des deux équipes sont en vie.
while (
  axeWarrior.isAlive() &&
  spearWarrior.isAlive()
) {
  // à chaque boucle chaque guerrier attaque son adversaire
    axeWarrior.attack(spearWarrior);
    spearWarrior.attack(axeWarrior);
    console.log('axe point de vie' + axeWarrior.life);
    console.log("spear point de vie" + spearWarrior.life);
}
    console.log("fin du combat spear :" + spearWarrior.life);
    console.log("fin du combat axe :" + axeWarrior.life);

// on détermine ensuite le résultat du combat : Si l'un est mort alors l'autre est déclaré gagnant et inversément,
// Si les 2 sont morts, c'est un match nul.
if (!axeWarrior.isAlive() && spearWarrior.isAlive()) {
  console.log(axeWarrior.name + " win");
} else if (!spearWarrior.isAlive() && axeWarrior.isAlive()) {
  console.log(spearWarrior.name + " win");
} else {
  console.log("It's a draw.");
}

// création avec la méthode constructor de la classe warrior qui permet de créer des objets possédant des propriétés name, power et life
export class Warrior {
  constructor(name, power, life) {
    this.name = name;
    this.power = power;
    this.life = life;
  }

  // Méthode qui permet de réduire la vie de l'opposant
  attack(opponent) {
    opponent.life -= this.power;
  }

  // méthode qui permet de savoir si le warrior est vivant ou non
  isAlive() {
    if (this.life <= 0) {
      return false;
    } else {
      return true;
    }
  }
}

// création de la classe WarriorAxe qui hérite de la classe Warrior
export class WarriorAxe extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorSword) {
      // instanceof est utilisé pour vérifier le type d'objet de l'opposant avant d'appliquer des règles spécifiques au type d'arme
      // ici si l'opposent est de type WarriorSword alors on applique la nouvelle méthode 
      opponent.life -= this.power * 2;
    }
    // sinon on applique la méthode du parent
    else {
      super.attack(opponent);
    }
  }
}

// création de la classe WarriorSword qui hérite de la classe Warrior
export class WarriorSword extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorSpear) {
      opponent.life -= this.power * 2;
    } else {
      super.attack(opponent);
    }
  }
}

export class WarriorSpear extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorAxe) {
      opponent.life -= this.power * 2;
    } else {
      super.attack(opponent);
    }
  }
}
